#lang pollen

<header>
<h1>Electromagnetismo</h1>
<h2>¿Qué objetos geométricos usar?</h2>
<pre>Cristian F. Guajardo Yévenes
Actualizado: 2022-01-08
Inicial: 2021-12-20</pre>
</header>


<div style="display:none">
$$ \let\gdef\def $$
</div>

<div style="display:none">
$$
\gdef\vect#1{\boldsymbol{#1}}
\gdef\elem#1#2{\underset{#1}{#2}}
\gdef\progr#1{\left] #1 \right]}
\gdef\progl#1{\left[ #1 \right[}
\gdef\regr#1{\left\rangle #1 \right\rangle}
\gdef\regl#1{\left\langle #1 \right\langle}
\gdef\geomr#1{\left\rbrace #1 \right\rbrace}
\gdef\geoml#1{\left\lbrace #1 \right\lbrace}
\gdef\dgeomr#1{\rBrace #1 \rBrace}
\gdef\dgeoml#1{\lBrace #1 \lBrace}
\gdef\coef{\vdash}
\gdef\coei{\dashv}
\gdef\dualf{\succ}
\gdef\duali{\prec}
\gdef\ddualf{\curlyeqsucc}
\gdef\dduali{\curlyeqprec}
\gdef\adual{\blacklozenge}
\gdef\adualf{\blacktriangleright}
\gdef\aduali{\blacktriangleleft}
$$
</div>


Ya sabemos que la cantidad de movimiento y la fuerza se pueden representar en el
álgebra geométrica, de mejor forma que en el álgebra vectorial, usando líneas en
vez de vectores.

En los casos de los campos eléctrico y magnético ¿cuáles serían los objectos con
los que se representan en el álgebra geométrica?.
Usando resultados de dos casos conocidos, uno para la fuerza eléctrica y otro
para la fuerza magnética, se obtiene que el campo eléctrico, al igual que en el
álgebra vectorial, está bien representado por un vector de puntos (punto cerca
del infinito), mientras que el campo magnético, a diferencia que en el álgebra
vectorial, está mejor representado por un vector de líneas (línea cerca del infinito).


Campo eléctrico
===============

Imaginar el caso en que una partícula con masa $m$ y carga $q$ se encuentra en
$\elem{1}{x}(t)$ dentro de un campo eléctrico constante y uniforme.

Experimentalmente se ve que la partícula se mueve en línea recta

$$ \elem{1}{x}(t) = \elem{1}{\vect{a}} t^{2} $$

con una aceleración constante que se puede medir experimentalmente

$$ \elem{1}{\vect{a}} = \frac{qE}{m} \elem{1}{\vect{\beta}_{2}} $$

y corresponde a una fuerza eléctrica constante dada por

$$ \elem{2}{f} = qE \progr{ \elem{1}{x}(t) \elem{1}{\vect{\beta}_{2}} } $$

donde $E$ es una constante de proporcionalidad, que corresponde a la intensidad
del campo eléctrico, y está medida en $\mathrm{N}/\mathrm{C} = \mathrm{V}/\mathrm{m}$.

Arreglando la expresión para fuerza

$$ \elem{2}{f} = \progr{ q\elem{1}{x}(t) \\, E \elem{1}{\vect{\beta}_{2}} } $$

se tiene que resulta de conectar el punto ponderado $q\elem{1}{x}(t)$ con el
vector de puntos $E \elem{1}{\vect{\beta_{2}}}$.

Esto sugiere que el campo eléctrico es un vector de puntos

$$ \elem{1}{\vect{E}} = E \elem{1}{\vect{\beta_{2}}} $$

y por lo tanto la fuerza eléctrica queda dada por

$$ \elem{2}{f} = \progr{ q\elem{1}{x}(t)  \elem{1}{\vect{E}} } $$


Campo magnético
===============

Imaginar el caso en que una partícula con masa $m$ y carga $q$ se mueve con
velocidad constante $v$ en un campo magnético constante y uniforme.

Experimentalmente se ve que la partícula se mueve formando trayectorias
circulares de radio $R$

<div>
$$
\elem{1}{x}(\theta)
= \elem{1}{\beta_{1}}
+ R \elem{1}{\vect{r}}(\theta)
$$
</div>

con una aceleración

<div>
$$
\elem{1}{\vect{a}}(\theta)
= -\frac{qvB}{m} \elem{1}{\vect{r}}(\theta)
$$
</div>

donde $\elem{1}{\vect{r}}$ es el vector de dirección radial

<div>
$$
\elem{1}{\vect{r}}(\theta)
= \elem{1}{\vect{\beta}_{2}} \cos\theta
+ \elem{1}{\vect{\beta}_{3}} \sin\theta
$$
</div>

y $B$ es una constante de proporcionalidad que corresponde a la intensidad del
campo magnético, medida en $\mathrm{N\\,s}/\mathrm{C\\,m} = \mathrm{V\\,s}/\mathrm{m}^2$
(en unidades derivadas $\mathrm{Wb}/\mathrm{m}^{2} = \mathrm{T}$).

De esta forma la partícula siente una fuerza magnética dada por

$$ \elem{2}{f}(\theta) = -qvB \progr{ \elem{1}{x}(\theta) \elem{1}{\vect{r}}(\theta) } $$

Arreglando la expresión para fuerza

$$ \elem{2}{f}(\theta) = \progr{ q\elem{1}{x}(\theta) \\, (-vB) \elem{1}{\vect{r}}(\theta) } $$

se tiene que resulta de conectar el punto ponderado $q\elem{1}{x}(\theta)$ con
el vector $-vB \elem{1}{\vect{r}}(\theta)$.

A su vez, el vector $-vB \elem{1}{\vect{r}}(\theta)$ debe ser el resultado de
operar la velocidad de la partícula

$$ \elem{1}{\vect{v}}(\theta) = v \elem{1}{\vect{\tau}}(\theta), $$

donde $\elem{1}{\vect{\tau}}(\theta)$ corresponde al vector de dirección tangente

<div>
$$
\elem{1}{\vect{\tau}}(\theta)
= -\elem{1}{\vect{\beta}_{2}} \sin\theta
+ \elem{1}{\vect{\beta}_{3}} \cos\theta,
$$
</div>

con algún elemento $\elem{?}{\vect{B}}$ (de grado aún no conocido).

Sin embargo, la operación entre $\elem{1}{\vect{v}}(\theta)$ y $\elem{?}{\vect{B}}$
no puede ser una simple conexión.
Si $\elem{0}{B} = -B \elem{0}{\alpha}$ fuera un objeto vacío, la conexión produciría
un vector, pero en la dirección equivocada

<div>
$$
\progr{ \elem{1}{\vect{v}}(\theta) \elem{0}{B} }
= -vB \elem{1}{\vect{\tau}}(\theta)
\neq -vB \elem{1}{\vect{r}}(\theta)
$$
</div>

Si $\elem{?}{\vect{B}}$ fuera un punto (finito o al infinito) o algún objeto mayor,
la conexión $\progr{ \elem{1}{\vect{v}}(\theta) \elem{?}{\vect{B}} }$ entregaría
como resultado una línea (finita o al infinito) o un objeto mayor que este.

¿Qué pasa entonces con $-vB \elem{1}{\vect{r}}(\theta)$ si probamos algún dual?

<table>
  <tr>
    <td> </td>
    <th> Espacio plano </th>
    <th> Espacio sólido </th>
  </tr>
  <tr>
    <td> $-vB \elem{1}{\vect{r}}(\theta)$ </td>
    <th> $N=3$ </th>
    <th> $N=4$ </th>
  </tr>
  <tr>
    <th>$-vB \aduali \adualf \elem{1}{\vect{r}}(\theta)$</th>
    <td>$vB \aduali \progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\tau}}(\theta) }$</td>
    <td>$vB \aduali \progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\tau}}(\theta) \elem{1}{\vect{\beta_{4}}} }$</td>
  </tr>
  <tr>
    <th>$-vB \adualf \aduali \elem{1}{\vect{r}}(\theta)$</th>
    <td>$vB \adualf \progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\tau}}(\theta) }$</td>
    <td>$-vB \adualf \progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\tau}}(\theta) \elem{1}{\vect{\beta_{4}}} }$</td>
  </tr>
</table>

usando $\elem{1}{\vect{v}}(\theta) = v \elem{1}{\vect{\tau}}(\theta)$ para
simplificar las expresiones anteriores

<table>
  <tr>
    <td> </td>
    <th> Espacio plano </th>
    <th> Espacio sólido </th>
  </tr>
  <tr>
    <td> $-vB \elem{1}{\vect{r}}(\theta)$ </td>
    <th> $N=3$ </th>
    <th> $N=4$ </th>
  </tr>
  <tr>
    <th> $-vB \aduali \adualf \elem{1}{\vect{r}}(\theta)$ </th>
    <td> $\aduali \progr{ \elem{1}{\vect{v}}(\theta)  (-B\elem{1}{\beta_{1}}) }$ </td>
    <td> $\aduali \progr{ \elem{1}{\vect{v}}(\theta)  (-B \elem{1}{\beta_{1}} \elem{1}{\vect{\beta_{4}}}) }$ </td>
  </tr>
  <tr>
    <th> $-vB \adualf \aduali \elem{1}{\vect{r}}(\theta)$ </th>
    <td> $\adualf \progr{ \elem{1}{\vect{v}}(\theta)  (-B\elem{1}{\beta_{1}}) }$ </td>
    <td> $\adualf \progr{ \elem{1}{\vect{v}}(\theta)  (B \elem{1}{\beta_{1}} \elem{1}{\vect{\beta_{4}}}) }$ </td>
  </tr>
</table>

La primera fila de la tabla anterior es una opción posible, pero tiene un
pequeño problema:
Sugiere que $\elem{?}{\vect{B}}$ es un objeto diferente en espacios con
dimensiones diferentes.
En el caso del espacio plano, sugiere que $\elem{?}{\vect{B}}$ es un punto
$-B\elem{1}{\beta_{1}}$.
En el caso del espacio sólido, sugiere que $\elem{?}{\vect{B}}$ es una línea
$-B\progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\beta_{4}}} }$.
Esta diferencia no debe ser.
La forma de $\elem{?}{\vect{B}}$ no debe depender
del espacio en consideración para modelar el mismo problema.

¿Qué pasa entonces si ahora probamos con algún dual para $-B\elem{1}{\beta_{1}}$
y $-B\progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\beta_{4}}} }$?

<table>
  <tr>
    <td>  </td>
    <th> Espacio plano </th>
    <th> Espacio sólido </th>
  </tr>
  <tr>
    <td> $\aduali \elem{?}{\vect{B}}$, $\adualf \elem{?}{\vect{B}}$ </td>
    <th> $N=3$ </th>
    <th> $N=4$ </th>
  </tr>
  <tr>
    <th> $-B \aduali \progr{ \elem{1}{\vect{\beta_{2}}} \elem{1}{\vect{\beta_{3}}} }$ </th>
    <td> $-B \aduali \adualf \elem{1}{\beta_{1}}$ </td>
    <td> $-B \aduali \adualf \progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\beta_{4}}} }$ </td>
  </tr>
  <tr>
    <th> $-B \adualf \progr{ \elem{1}{\vect{\beta_{2}}} \elem{1}{\vect{\beta_{3}}} }$ </th>
    <td> $-B \adualf \aduali \elem{1}{\beta_{1}}$ </td>
    <td> $-B \adualf \aduali \progr{ \elem{1}{\beta_{1}} \elem{1}{\vect{\beta_{4}}} }$ </td>
  </tr>
</table>

La tabla anterior sugiere que el campo magnético debe ser un vector de líneas
(línea cerca de infinito)

$$ \elem{2}{\vect{B}} = -B \progr{ \elem{1}{\vect{\beta_{2}}} \elem{1}{\vect{\beta_{3}}} } $$

para la cual se cumple

$$ \aduali \elem{2}{\vect{B}} = \adualf \elem{2}{\vect{B}} $$

Finalmente la fuerza magnética resulta

<div>
$$
\elem{2}{f}(\theta)
= \progr{ q\elem{1}{x}(\theta)  (-vB) \elem{1}{\vect{r}}(\theta) }
= \progr{ q\elem{1}{x}(\theta) \aduali \progr{ \elem{1}{\vect{v}}(\theta) \aduali \elem{2}{\vect{B}} } }
= \progr{ q\elem{1}{x}(\theta) \aduali \progr{ \elem{1}{\vect{v}}(\theta) \adualf \elem{2}{\vect{B}} } }
$$
</div>

cuya operación interna se puede convertir en un producto regresivo

<div>
$$
\elem{2}{f}(\theta)
= \progr{ q\elem{1}{x}(\theta) \regr{ \aduali \elem{1}{\vect{v}}(\theta) \elem{2}{\vect{B}} } }
$$
</div>

o equivalentemente

<div>
$$
\elem{2}{f}(\theta)
= \progl{ \regl{ \elem{2}{\vect{B}} \aduali \elem{1}{\vect{v}}(\theta) } q\elem{1}{x}(\theta) }
$$
</div>
