<header>
# Álgebra geométrica de los elementos rectos
</header>

# elementos y objetos

dos puntos determinan una línea

$$ p_{1} \wedge p_{2} $$

una línea y un punto determinan un plano

$$ (p_{1} \wedge p_{2}) \wedge p_{3} $$


# Los elementos

De todos los elementos geométricos posibles, solo vamos a considerar algunos.

Empecemos con el elemento geométrico más simple: El punto.

Ahora, dos puntos determinan de forma unívoca una línea recta infinita que pasa a través de ellos.

Ahora, una línea y un punto determinan de forma unívoca una superficie recta (plana) infinita que los conecta.

También, una superficie recta y un punto determinan de forma unívoca un sólido recto e infinito que los conecta.

Podríamos continuar esta secuencia con elementos de orden mayor, pero ya no nos sería tan fácil saber que elementos son.

También podemos generar esta secuencia en orden inverso.

Dos sólidos rectos e infinitos determinan de forma unívoca la superficie recta e infinita que tienen en común.

Dos superficies rectas e infinitas determinan de forma unívoca la línea recta e infinita que tienen en común.

Una superficie recta y una línea recta determinan de forma unívoca el punto que tienen en común.


# 2021-01-2x

Cada objeto es único en el espacio que ocupa.

Digamos que $\rho$ es dicho objeto.

A este único objeto le podemos asociar un número arbitrario. Se simbolizara dicha combinación entre un número y el objeto interponiendo $\wedge$.

Por definición diremos que

$$ 1 \wedge \rho := \rho $$

Así tenemos que los objetos $1 \wedge \rho$ y $3 \wedge \rho$ ocupan el mismo espacio, pero tienen peso diferente.

Si a este objeto le asociamos un número, entonces ya deja de ser único. 

Este número puede interpretarse como una propiedad adicional del objeto. A esta propiedad le llamaremos *peso* en general, pero podía interpretarse también como masa, carga, color u otro.


# Ideas muy antiguas

## D-espacio (físico)

## k-objetos rectos (e infinitos)

## En 3-espacio físico

Un 3-objeto recto ocupa toda la dimensionalidad de este espacio físico.
No importa a dónde se mueva ni hacia dónde se gire este objeto, al final de todas estas operaciones, siempre tendremos cono resultado el mismo objeto que al principio.
Es decir, tendremos el mismo objeto con la misma orientación.

De forma diferente ocurre con 2,1,0-objetos rectos, ya que en este caso es posible cambiar completamente su orientación.
Por ejemplo, se puede tomar un 2,1-objeto y cambiarle su inclinación, resultando así un objeto diferente del inicial. También se puede tomar un 2,1,0-objeto y cambiar de ubicación, con lo que también resulta un objeto diferente del original.

## x

Empecemos con un espacio físico de cero dimensiones. El único objeto geométrico que podemos poner allí es un punto. Este punto llena toda la dimensionalidad de este espacio.

## Espacio físico de $D$ dimensiones

Es un espacio que podemos percibir o medir físicamente, en el cual nos podemos mover en $D$ direcciones independientes.

## Elementos geométricos

Hay elementos que ocupan toda la dimensionalidad del espacio geométrico en cuestión.

Hay elementos de menor orden, disminuyendo hasta (segmentos de) líneas.
Todos ellos también son escalables.

Vamos a requerir que los puntos y los elementos vacíos también sean escalables.

Estos elementos (excepto los elementos vacíos) tienen una posición en el espacio y tienen una orientación.
Dichos elementos son escalables y se pueden sumar.

## Números

Tienen operaciones bien definidas, pero no representan objetos geométricos.

## Orientación

[Orientación (geometría)](https://es.wikipedia.org/wiki/Orientación_(geometría))

Una orientación de un objeto en el espacio es cada una de las posibles elecciones para colocarlo sin cambiar un punto fijo de referencia.

[Orientation (geometry)](https://en.wikipedia.org/wiki/Orientation_(geometry))

it refers to the imaginary rotation that is needed to move the object from a reference placement to its current placement. A rotation may not be enough to reach the current placement. It may be necessary to add an imaginary translation, called the object's location (or position, or linear position). The location and orientation together fully describe how the object is placed in space

[Orientation (vector space)](https://en.wikipedia.org/wiki/Orientation_(vector_space))

It is a standard result in linear algebra that there exists a unique linear transformation A : V → V that takes b1 to b2. The bases b1 and b2 are said to have the same orientation (or be consistently oriented) if A has positive determinant; otherwise they have opposite orientations.

[en castellano es una traducción de la página en inglés]

## $D$-objetos

Son objetos que ocupan toda la dimensionalidad del espacio físico en cuestión.

## $k$-objetos

## $k$-objetos ponderados

