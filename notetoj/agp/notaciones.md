Aquí hay posibles notaciones para los productos progresivo y regresivo, y también para el complemento o dual

Caso de productos

$$ \rangle \rangle a b \rangle c \rangle = \rangle a b c \rangle = \rangle a \rangle b c \rangle \rangle $$

$$ ]] a b ] c ] = ] a b c ] = ]] a b ] c ] $$

| l | r | l | r |
|---|---|---|---|
| $(ba($ | $)ab)$ | $ba$ | $ab$ |
| $\[ba\[$ | $\]ab\]$ | $\llbracket ba \llbracket$ | $\rrbracket ab \rrbracket$ |
| $\lbrace ba \lbrace$ | $\rbrace ab \rbrace$ | $\lBrace ba \lBrace$ | $\rBrace ab \rBrace$ |
| $\lang ba \lang$ | $\rang ab \rang$ | | |
| $\langle ba \langle$ | $\rangle ab \rangle$ | | |

Posibles notaciones para la cobase, complemento y dual

| l | r | l | r | c |
|---|---|---|---|---|
| $\rceil a$ | $\lceil a$ | $\rfloor a$ |  $\lfloor a$ |
| $\urcorner a$ | $\ulcorner a$ | $\lrcorner a$ |  $\llcorner a$ | $\sqcap a$ $\sqcup a$ |
| $\dashv a$ | $\vdash a$ | $\dashV$ | $\Vdash a$ |
| $\lessdot a$ | $\gtrdot a$ | | | $\cdot a$ |
| $\prec a$ | $\succ a$ | $\curlyeqprec a$ | $\curlyeqsucc a$ |
| $\sqsubset a$ | $\sqsupset a$ | | | $\box a$ $\square a$ $\blacksquare a$ |
| $\subset a$ | $\supset a$ | $\Subset a$ | $\Supset a$ | $\circ a$ $\bullet a$ |
| $\ltimes a$ | $\rtimes a$ | | | $\bowtie a$ $\join a$ |
| $\triangleleft a$ | $\triangleright a$ |  |  |
| $\vartriangleleft a$ | $\vartriangleright a$ | $\blacktriangleleft a$| $\blacktriangleright a$ |
| $\lhd a$ | $\rhd a$ | | | $\diamond a$ $\diamonds a$ $\diamondsuit a$ $\Diamond a$ $\lozenge a$ $\blacklozenge a$ |
