publish:  ## Render notetoj in 'public' folder
	raco pollen render notetoj
	raco pollen publish notetoj public


clean-render:  ## Clean rendered files in 'notetoj' folder
	find notetoj -type f -name *.css -print -delete
	find notetoj -type f -name *.xhtml -print -delete
	find notetoj -type f -name *.html -print -delete

clean-compiled:  ## Clean Racket's 'compiled' folder
	find notetoj -type d -name compiled -print -exec rm -rf {} +

clean-hidden:  ## Remove hidden backup files *.*~
	find notetoj -type f -name *.*~ -print -delete

clean-publish:  ## Remove 'public' folder
	rm -rf public

clean-notetoj: clean-render clean-compiled  ## clean-render clean-compiled

clean-all: clean-notetoj clean-publish clean-hidden  # clean-notetoj clean-publish clean-hidden


.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
