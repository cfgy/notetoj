Notetoj
=======

<dl>
    <dt> Página web </dt>
    <dd> <a href="https://cfgy.gitlab.io/notetoj">https://cfgy.gitlab.io/notetoj</a> </dd>
    <dt> Código fuente </dt>
    <dd> <a href="https://gitlab.com/cfgy/notetoj">https://gitlab.com/cfgy/notetoj</a> </dd>
</dl>

Estas son pequeñas notas, que escribo mayormente en mi celular, cuando tengo
algún momento libre. Me gusta la ciencia, así que es probable que en su mayoría,
estas traten temas relacionados con matemática, física u otros temas científicos.

La intención es que **me sirvan** para aclarar y decantar los temas que estudio,
y en principio, no están dirigidas a segundas o terceras personas. De todas
formas las dejo abiertas al público interesado, para que las lea en caso que
también les sea de utilidad.

Después que estas ideas hayan sido digeridas un poco, es posible que pasen a mi
proyecto de notas más completas [Notoj], que sí está destinado a exponer un tema
para que otra persona lo entienda.

[Notoj]: https://cfgy.gitlab.io/notoj

Tecnología
----------

Estas pequeñas notas probablemente estén escritas en su mayoría en formato
[Markdown], que es lo que me resulta más comodo y rápido para procesar ideas y
escribir especialmente matemática en el celular.

Preferiría usar [reST] en vez de [Markdown], pero el único editor de textos que
he encontrado para android que soporta matemática, que es [Markor] usando
[KaTeX], lamentablemente no soporta [reST].

Finalmente, uso [Pollen] para convertir de forma estática las notas escritas en
[Markdown] a formato [XHTML5].

[KaTeX]: https://katex.org/
[Markdown]: https://www.markdownguide.org/
[Markor]: https://github.com/gsantner/markor
[Pollen]: https://docs.racket-lang.org/pollen/
[reST]: https://docutils.sourceforge.io/rst.html
[XHTML5]: https://en.wikipedia.org/wiki/XHTML#XHTML5


Licencia
--------

© 2020 Cristian F. GUAJARDO YÉVENES.

*Notetoj*, o cualquiera de sus archivos, puede ser redistribuido y modificado
de acuerdo a los términos de la [Licencia Pública General de GNU](https://www.gnu.org/licenses/licenses.es.html#GPL)
en su [version 3](https://www.gnu.org/licenses/gpl-3.0.en.html) o cualquier version posterior.

*Notetoj*, or any of its files, can be redistributed and modified under the terms
of the [GNU General Public License](https://www.gnu.org/licenses/#GPL) in its
[version 3](https://www.gnu.org/licenses/gpl-3.0.en.html) or any later version.
